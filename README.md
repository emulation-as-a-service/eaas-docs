# Documentation for EaaS

The documenation can be generated with the *Sphinx* toolchain in multiple formats.

For more information, see also:
* [Sphinx Documentation](http://www.sphinx-doc.org/en/stable/contents.html)
* [reStructuredText Documentation](http://docutils.sourceforge.net/rst.html)


## Sphinx Installation

Sphinx version is expected to be 1.3 or later. If your distribution provides an up-to-date package,
install that one. E.g. for installing on Ubuntu:
```
$ apt-get install python-sphinx
```

Alternatively, latest version of Sphinx can also be installed from PyPI with:
```
$ apt-get install pip
$ pip install Sphinx
```

The "Read The Docs" theme must also be installed. This can be done with:
```
$ apt-get install python-sphinx-rtd-theme
```

or using PyPI:
```
$ pip install sphinx_rtd_theme
```


## Generating Documentation

To build the documentation in HTML format, simply run:
```
$ make html
```

The generated static HTML files can then be found in `build` directory.

