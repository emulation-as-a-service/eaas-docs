from ubuntu
run apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get install -y \
  make python3-sphinx python-sphinx-rtd-theme
workdir /src
copy . .
run make html && mv build /opt/eaas-docs
