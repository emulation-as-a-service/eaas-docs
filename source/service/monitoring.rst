.. monitoring

.. include:: ../reference/field-type-defs.txt

Monitoring
==========

The EaaS gateway, i.e. the resource provides a monitoring API to retrieve statistics
about its current state.

Usage Examples
--------------

.. note:: all API calls shoud include an additional HTTP header ``X-Admin-Access-Token: <ACCESS-TOKEN>``, with
   the access token defined in the ``eaas-config.yaml`` as ``clustermanager.admin_api_access_token``.

Retrieve all active cluster names::

   GET /eaas/api/v1/clusters

Retrieve current state of a specific cluster::

   GET /eaas/api/v1/clusters/<CLUSTER-NAME>


.. note:: the following examples retrieve subsets of the cluster state.

Retrieve the current state of a node provider::

   GET /eaas/api/v1/clusters/<CLUSTER-NAME>/providers/<PROVIDER-NAME>

Retrieve stats of a provider::

   GET /eaas/api/v1/clusters/<CLUSTER-NAME>/providers/<PROVIDER-NAME>/metrics

Retrieve information of a provider's node pool::

   GET /eaas/api/v1/clusters/<CLUSTER-NAME>/providers/<PROVIDER-NAME>/node-pool

Filter
^^^^^^

The API further allows to filter a request for specific subsets of a provider.

To retrieve only config and metrics::

   GET /eaas/api/v1/clusters/<CLUSTER-NAME>/providers/<PROVIDER-NAME>?fields=config,metrics

To get provider stats without its config::

   GET /eaas/api/v1/clusters/<CLUSTER-NAME>/providers/<PROVIDER-NAME>?fields=!config


Examples
^^^^^^^^

A full example of an idle google cloud EaaS gateway:

.. literalinclude:: examples/eu-gw-state.json
   :language: json


A provider example for active blade gateway (currently two active sessions):

.. literalinclude:: examples/blades-provider-2.json
   :language: json
