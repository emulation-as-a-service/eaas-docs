.. EaaS docs


Emulation-as-a-Service Documentation
====================================

The documentation is organized into multiple parts.


.. toctree::
   :maxdepth: 1

   install/docker
   install/build
   service/integration
   service/monitoring

.. toctree::
   :caption: EaaS Service Examples
   :maxdepth: 1

   service/cloud

.. toctree::
   :caption: Workflows / UI
   :maxdepth: 1

   usage/demo-ui-user
   usage/demo-ui-admin

.. toctree::
   :caption: EaaS Reference
   :maxdepth: 2

   reference/reference