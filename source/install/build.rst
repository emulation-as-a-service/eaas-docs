

Build (Compile)
===============

You may skip this section if you plan to use prepackaged Docker images.

Build EaaS Server
-----------------

Build Requirements
^^^^^^^^^^^^^^^^^^
- Java 8
- Maven 3.3 or newer

Download Code
^^^^^^^^^^^^^

Create an account at https://git.eaas.uni-freiburg.de.

Download current master as `ZIP <https://git.eaas.uni-freiburg.de/eaas/bw-fla/archive/master.zip>`_,
`TAR.GZ <https://git.eaas.uni-freiburg.de/eaas/bw-fla/archive/master.tar.gz>`_ or::

   git clone https://git.eaas.uni-freiburg.de/eaas/bw-fla.git

Build
^^^^^

In ``bw-fla/src``::

   mvn clean install

Deploy (Manually)
^^^^^^^^^^^^^^^^^

Copy ``ear/target/eaas-server.ear`` to the Docker deployments folder and map it as Docker volume to ``/eaas/deployments``
(see also Setup).

Deploy (Develoment)
^^^^^^^^^^^^^^^^^^^

With a running ``eaas/eaas-dev`` instance Maven hot deployment is possible. Run::

   mvn wildfly:deploy


