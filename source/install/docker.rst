.. Docker setup

.. include:: ../reference/field-type-defs.txt

Setup
=====

In order to simplify deployment of EaaS, all components are available as Docker instances.
The following applies to all installation types i.e. desktop and server installation.

Prerequisites
-------------

.. _docker_install_section:

Install Docker
^^^^^^^^^^^^^^
Follow the official instructions:

- `Docker for Windows <https://docs.docker.com/docker-for-windows/install/>`_
- `Docker for Mac OS <https://docs.docker.com/docker-for-mac/install/>`_
- `Docker for Linux (Desktop & Server) <https://docs.docker.com/engine/installation/#cloud>`_

.. note:: With Docker for Mac there is a known issue regarding Java and the Docker storage backend. See https://stackoverflow.com/questions/41022393/mount-point-not-found for more details.

Install Docker Compose
^^^^^^^^^^^^^^^^^^^^^^
Docker compose is automatically installed for most Windows and Mac users. Linux users should follow `these instructions
<https://docs.docker.com/compose/install/>`_.

Preparation
-----------

Prepare Host Environment
^^^^^^^^^^^^^^^^^^^^^^^^
Usually, no additional modifications to the host environment are required. However,
to enable certain EaaS features additional steps are required:

- **KVM support**: make sure that the Docker user is able to read/write ``/dev/kvm``.
- **Writable Shared Folders**: make sure the the Docker user has write permission to shared folders.
- **SELinux**: if SELinux is enabled, make sure to allow mapping low memory addresses `
  (required for Sheepshaver): run ``sudo setsebool -P mmap_low_allowed 1``


.. note:: KVM (virtualization) is only supported on native Linux hosts.

Prepare Docker Compose Start Script
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The EaaS instance should be started through a `Docker Compose <https://docs.docker.com/compose/compose-file/>`_ file.
A minimal ``docker-compose.yaml`` file should look like:

.. literalinclude:: examples/docker-compose-minimal.yaml
	:language: yaml

- ``services.eaas.image``: defines the EaaS Docker image to be used. In the example a desktop
  image is selected. If the image is not locally available, Docker will pull the image from
  `Docker Hub <https://hub.docker.com/u/eaas/>`_. The same applies if the local image is outdated.
- ``services.eaas.container_name``: the name for this instance to be used as reference for other
  Docker commands (e.g. start/stop/restart/exec/...)
- ``services.eaas.privileged``: start Docker instance in privileged mode. This has to be `true`
  if all emulators should be usable. **Note:** The privilege mode
  allows processes running within the Docker environment to access host resources and in fact
  disables most of the Docker's isolation/security features. However, the EaaS Docker has been
  designed to simplify setup and usage.
- ``port``: define host port for the EaaS service. By default the EaaS docker exposes the EaaS service
  through port 80 (right hand value). This port can be mapped to host port, in this example 8080
  (left value)

.. note:: If a public service (frontend) is to be deployed, make sure to add ``hostname: your.fqdn``
  to the ``docker-compose.yaml``. Under certain circumstances (combined public facing frontend and
  emucomp) Docker has issues with routing to itself using the external network interface. By adding the
  hostname, routing to the local instance is done through the local interface.

Ready-made Docker Images
^^^^^^^^^^^^^^^^^^^^^^^^

On `Docker Hub <https://hub.docker.com/u/eaas/>`_ we publish regularly ready-made images for testing and
production. Currently the following images (flavors) are supported:

**Development and Testing**

- ``eaas/eaas-dev``: basic container with all system dependencies installed. This container is mainly
  for testing and development purposes. To run, a ``deployments`` directory has to be mapped (see below).
  Also no web-UI is included.

- ``eaas/eaas-master``: builds on ``eaas/eaas-dev`` but contains a nightly built ``eaas-server.ear``
  deployment, i.e. remove the ``deployments`` mapping line form the docker-compose startup file (see below).

- ``eaas/eaas-master-ui`` builds on ``eaas/eaas-master`` and contains additionally a nightly snapshot of the
  ``demo-ui``, i.e. remove the ``demo-ui`` mapping line from the docker-compose startup file (see below). Instead,
  mapping of the UI configuration is required (see also :ref:`Demo UI <demo_ui>`, :ref:`Admin Demo UI <demo_admin_ui>`), e.g. add to the docker mapping::

    - ./demo-ui/config.json:/eaas/demo-ui/config.json
    - ./demo-ui/admin/config.json:/eaas/demo-ui/admin/config.json


.. note:: All **master** based docker images are automated builds from our GIT. These images are for development or experimental
   usage only.


**Releases**


Preparing the EaaS Runtime
^^^^^^^^^^^^^^^^^^^^^^^^^^

The EaaS docker instance allows to map user defined directories from the docker host and make
them available within the EaaS Docker environment. A complete example with all currently
usable mappings:

.. literalinclude:: examples/docker-compose-volumes.yaml
	:language: yaml

Entries under ``service.eaas.volumes`` allow to map a local path (left hand side, in
this example all host paths are relative to the current working directory). Right-hand side
values are fixed.

.. note:: This section should be moved to the References section.


- ``deployments``: map this directory to deploy a local built ``eaas-server.ear``.
  Most available docker images already a deployment, to be used for development container (eaas-dev).
- ``emil-environments``: **deprecated** proxy metadata for local environment.
  Currently used by the demo-ui. Not necessary for the minimal-ui, REST usage.
- ``emil-object-environment``: **deprecated** (similar to emil-environments).
- ``log``: contains log files.
- ``config``: contains configuration files
- ``classification-cache``: **deprecated** cache object classification permanently.
  Classification data is always cached, but will be only permanently (e.g. outside of
  the Docker environment.
- ``import``: image import from the local file system is restricted to this folder
- ``export``: **deprecated** the demo-ui is able to export images to this folder
- ``software-archive``: local software archive data (see Software Archive)
- ``demo-ui``: provide demo-ui html root
- ``image-archive``: local file-based image archive (see Image Archive)
- ``objects``: local file-based object archive (see Object Archive)

To adopt default mapping either the run script or the docker-compose.yaml can be modified. See section :ref:docker_run_.

For certain scenarios the creation of a working directory can be omitted. See section ref:zero_conf_.

.. note:: An empty image-archive can be downloaded `here <http://eaas.uni-freiburg.de/empty-image-archive.tgz>`_. Download and unpack.
  Follow :ref:`these instructions <import_image>` to import existing images.

Configuration
-------------

An EaaS instance can be run in various modes. To configure an instance create a ``eaas-config.yaml`` file
and set values appropriately. For most configuration values there are predefined defaults (see References section),
however, a minimal set configuration values is usually required.

EaaS framework consists of multiple submodules. Each of the submodules can be configured in the corresponding sections
in a file ``eaas-config.yaml``. A detailed description of submodule-specific configuration sections can be found
under :ref:`config-ref-section`.

Example: Standalone (Local) Instance
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This example configures a standalone instance bound to localhost:8080.

.. literalinclude:: examples/eaas-config.standalone.yaml
   :language: yaml

This instance the WS endpoints (sub modules) ``imagearchive``, ``objectarchive``. ``softwarearchive`` and ``eaasgw``
on http://localhost:8080.

Furthermore, this instance uses the `blade cluster` allocator (in EaaS terms, a blade cluster is a set of uniform
compute nodes) and configures an emulation component on localhost:8080 featuring 48 CPUs and 20480 Mb of memory.

.. note:: All DNS/IP addresses specified refer to the hosts network view.

   In most cases it is difficult or impossible for the EaaS code running inside the docker to
   identify its outside address. In particular the emulator nodes addresses should be reachable
   by the client browser, as the browser connects directly to the emulation node to access a running
   emulation session.


First Run
---------

Start the EaaS Instance
^^^^^^^^^^^^^^^^^^^^^^^
To start EaaS Framework run the ``docker-compose up`` in the directory containing the ``docker-compose.yaml`` file.

The startup of the system may take a few seconds. You should see something similar like:

.. literalinclude:: examples/start.log

If the demo-ui is deployed, open in the browser: http://localhost:8080/ (adjust port number if necessary).
Wait until the system is ready, click on “Administration” in the upper left.

Debug
^^^^^
If you keep the Docker terminal open, syslog messages will be displayed. Furthermore, in ``log`` directory (if mapped)
the application server logfile is available.

To explore the docker environment open another terminal and run
``$ docker exec -it eaas bash``
to get a root shell within the docker environment. In this example the running instance name is ``eaas``.
