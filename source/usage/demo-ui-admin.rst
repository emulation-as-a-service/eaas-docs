
.. _demo_admin_ui:


Administration UI
-----------------

The startup of the system may take a few seconds. Open in your browser:
http://localhost:8080/
Wait until the system is ready, click on “Administration” in the upper left.

.. image:: images/first-run.png
   :scale:  30%

The administration UI is currently integrated with in the user/demo UI. The source code is
in ``demo-ui/admin``. Technically the admin UI is an independet project and can be hosted independently, i.e.
by only the admin folder.

Configuration
^^^^^^^^^^^^^

A clean GIT checkout / download needs to be configured first (similar to the demo-ui). In the ``demo-ui/admin`` folder
create a new ``config.json``from the provided ``config.json.template``.

**Example**

.. literalinclude:: examples/admin-ui-config.json
  :language: json

- ``eaasBackendURL`` URL to the EaaS REST API
- ``stopEmulatorRedirectURL`` demo-ui landing page
- ``dashboardClusterAPIBaseURL`` url to the cluster manager api
- ``dashboardAccessToken`` API access key defined in ``eaas-config.yaml``

Environments Overview
^^^^^^^^^^^^^^^^^^^^^
A list  of available emulation environments is displayed (if images/environments are available,
e.g. by downloading the demo image archive).

.. image:: images/environments.png

To test if the EaaS framework works, choose an environment and click “configure environment”.
The environment should start and should be usable. Performance of the chosen environment depends on the
abilities of the host machine.

- Environments can be configured, e.g. network configuration, screen resolution and color depth, etc.
- An environment "configuration" will not create a new environment.
- Create a new environment by installing a software package to an existing environment.
- Export an environment to the Cloud or USB appliance.

.. _import_image:

Managing Base Environments
^^^^^^^^^^^^^^^^^^^^^^^^^^
This section describes workflows creating / importing emulation environments.Currently two workflows are supported:

- Import an existing disk image, either created with a different emulation (framework) or imaged from a physical hard disk
- Creation of a new environment, by installing an operating system on an empty (virtual) hard disk.

For both cases choose “create base environment” from the left menu of the environments overview page.

Also for both cases, in a first step choose an emulator template (currently labeled a “Choose System”).
This list represents technical environments, to be combined with an disk image.

.. image:: images/new-base-env.png

Import Disk Image
"""""""""""""""""

To import an existing image, enter the desired environment label / name (Environment Name) and fill out
the *Import Image from URL* input field. Currently two modes are supported:
- Copy the disk image to the emulation work dir’s import folder. In this case simply provide the filename of the image, without any paths.
- Provide a http:// URL, pointing to the image file. The file will be then downloaded.

Clicking the start button will start a preview of the imported environment

.. image:: images/import-image-test.png

Click on Save Changes to keep the imported environment. In this case the environment will be imported (copied) to the
image archive. The original image file will remain in the import folder and can be deleted.

.. note:: No changes to the original environment made during the preview session will be saved.
   In order to track environment development, additional changes need to be made in a separate step (see below).

**Example**:

- Dowload and unpack the ``Microsoft Windows NT 4.0 Workstation SP6 [VMware VM]`` image
- Copy the VMDK file to the configured import folder (avoid spaces in the image's file name)
- Within the import UI, choose a generic PC template (e.g. XXX) and input the images filename (no path / directory information is required.

.. image:: images/import-nt40.png

Example Images (Tested)
"""""""""""""""""""""""
.. note:: Download and unpack the readymade VM images. Use at your own risk!

- **Windows NT 3.5** https://winworldpc.com/product/windows-nt-3x
- **Windows NT 4 SP 6** https://winworldpc.com/product/windows-nt-40
- **MS-DOS 6.22** https://winworldpc.com/product/ms-dos/622
- **Windows 98 SE** https://winworldpc.com/product/windows-98 (requires ISO for hardware adaption)

Install Operating System
""""""""""""""""""""""""

In order to install an operating system, a software object marked as an installable operating system is required to be
present within the EaaS system. The process of ingesting an operating system is described in section "Maintaining Software Objects”.

To install an operating system a new disk has to be created, the disk’s size (in MB) is to be provided by the user.
Install from object chooses a pre-ingested operating system software object.

.. image:: images/os-install.png
   :scale:  70%

Once the session has started, the operating system can be installed. Multiple restarts of the environment are possible,
but do NOT use the Restart button during install. This button will reinitiate a new emulation session (with the same parameters)
but all previous installation steps will be lost.

.. image:: images/win98-install.png

If the installation was successful, make sure to shut down / power off the operating system properly. This avoids
unclean disk images and/or corrupted file systems. Also here, do NOT use the Stop button, but use the OS build in
functionality to shut down the OS. The Stop button will end the session without saving the newly created environment.
Once the environment has been shut down, click Save Changes to add the newly created environment to the environment list.
Also in this case it is not advisable to install and configure the environment in a single session. If only the base
installation is saved, any further configuration steps should be done individually to be able to track (and possible revert) changes.

Example Images (Tested)
"""""""""""""""""""""""
.. note:: Download and unpack, create add to object/software-archive

- **MS-DOS 6.22** https://winworldpc.com/product/ms-dos/622 (3.5" floppies)

Maintaining Environments
""""""""""""""""""""""""

Once a newly created or imported environment is functional this environment can be developed further.

.. image:: images/edit-environment.png

Managing Objects
^^^^^^^^^^^^^^^^

Usually, (digital) objects are maintained by dedicated object repositories. Therefore, EaaS provides adapters to
import/export object from/to such repositories and no dedicated UI to import and/or maintain objects.
In the standalone desktop version, however, digital objects can be maintained manually through a file system structure.
To make digital objects available within the EaaS framework:

#. Create an ``objects/`` directory within your EaaS working directory
#. Create a new subfolder under objects/ representing both the target folder of an
   individual object as well as the objects internal ID. Therefore, please avoid using folder names
   with spaces or exotic (non ASCII) characters.
#. Within the object subdir, create a further subdir, describing the objects individual media types.
   Currently, supported media types are:

   - ``iso/`` This folder should contain CDROM disk images.
   - ``disk/`` This folder should contain hard disk images.
   - ``floppy/`` This folder should contain floppy disk images.
   - ``file/`` This folder may contain arbitrary files (including subfolder). These files will be wrapped as
     ISO/CDROM image by default. Note: the folder will not be checked for updated files, i.e. the contents of this folder will
     not be “repackaged” automatically. To force, repackaging, delete the `__imported.iso` file in the ``iso/`` folder of the current object.

To set the default object to be inserted into an environment (in case there are multiple images for a media type)
create a properties file called ``resource.properties`` within the root folder of an individual object.

.. literalinclude:: examples/resource.properties

``defaultFile`` should contain the filename of the default image to be attached to an emulators drive if multiple files are available.

Available objects will be listed under: http://localhost:8080/.

.. image:: images/objects.png

.. note:: If you added an object while EaaS was running, the contents of the object archive need to be reloaded. Click "Settings" - "Sync Object Archives".

A right click on the object, starts the object characterization process (suggests an environment suitable for this
object - i.e. PRONOM file format ids).

.. image:: images/characterization.png

Each environment can be tested with the object. Objects can either be associated with an
emulation environment or a fixed object-environment can be created. For both cases, the object can be tested
with available environments:

- associated environments: in this case a list of suitable (either determined by the object characterization process or
  determined/edited manually) is maintained with the object. Environments are generic and may be associated with different
  objects.
- within the test session object-environments can be saved. An object-environment represents a combination of an emulation
  environment, with an object specific configuration and the object itself.

Maintaining Software-Objects (incl. Operating Systems)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In EaaS software (objects) are regular objects with additional meta-data.
Hence, to be able to use software objects (incl. operating systems) software needs to be ingested as an object first.

.. image:: images/software-overview.png

The Software menu entry will open a list of already ingested software objects. Click on Add new software to promote a
generic digital object to a software object.

.. image:: images/software-new.png

In the next step an object can be selected and additional metadata can be added. Tick Operating System to
indicate this object as an installable operating system. This implies that the object’s file (e.g. ISO or floppies)
are bootable.

