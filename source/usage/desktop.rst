.. EaaS Desktop

Installation
============
The EaaS desktop tool has been developed / designed to be run locally on the user’s desktop computer as an
appraisal or preparation utility. The tool is able to import and export environments to other presentation forms.

Prerequisites
-------------
A system with a current Docker installed. See :ref:`docker install <docker_install_section>`.

Preparation
-----------

Create a working directory.


