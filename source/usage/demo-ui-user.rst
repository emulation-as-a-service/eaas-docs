.. _demo_ui:

Demo UI
=======

The demo UI is an example implementation of the `EaaS RESTful API <http://eaas.uni-freiburg.de/restapi>`_.

User UI
-------

The demo-ui is javascript-based and self-contained and the GIT master is usually in sync with the EaaS REST
implementation. There are no specific technical requirements hosting the demo-ui, i.e. any basic web server
should work.

Configuration
^^^^^^^^^^^^^

A clean GIT checkout / download needs to be configured first. In the ``demo-ui/`` folder create a new ``config.json``
from the provided ``config.json.template``.

**Example**

.. literalinclude:: examples/user-ui-config.json
  :language: json

- ``eaasBackendURL`` URL to the EaaS REST API
- ``stopEmulatorRedirectURL`` demo-ui landing page

Catalog
^^^^^^^

The EMiL Demo provides a simple catalog example, simulating a web-based library catalog. The catalog is acting as a
starting point for users to access digital objects.

By choosing an object (left-click on the object or through the context menu), the EMiL access workflow is started.
The catalog system passes the object ID to the EMiL EaaS framework using a HTTP request.
The object is characterized on-the-fly, if no manually assigned emulation environments are found. The characterization
process, if successful, provides a (list of) suitable emulation environment(s) for the object.

.. image:: images/emil-ui-1.png

Emulation Access Landing Page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The result of the characterization process is a list of suitable rendering environments.
The emulation access landing-page suggests a specific environment. If there are more suitable environments
available (e.g. Windows/Mac hybrid CDs) the user is able to choose from a list of alternatives.

.. image:: images/emil-ui-2.png

Object Rendering
^^^^^^^^^^^^^^^^

By default, objects are rendered within the browser using HTML5 techniques. While the session is active, the user
is able to use the object interactively as well as to control the emulation system (e.g. create a screenshot,
change media if an object consists of multiple media and stop or restart a session).

.. image:: images/emil-ui-3.png

