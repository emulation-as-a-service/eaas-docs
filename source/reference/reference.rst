.. EaaS config reference


.. _config-ref-section:

Reference
=========

.. toctree::
  :maxdepth: 2

  configuration
  rest
  objectarchive
  metadata


