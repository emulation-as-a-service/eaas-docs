
EaaS Object Archive Adapter
---------------------------

The EaaS Object Archive facade allows the inclusion of multiple object archives (i.e. repositories). By implementing
the ``DigitalObjectArchive`` interface, custom adapters can be added. Multiple concurrent archives are supported.
Configuration files stored in the same directory as the ``eaas-config.yaml`` files will be automatically detected.

The Object Archive Adapater configuration files are currently encoded as JSON files and contain two common fields:

- ``type``: possible types ``FILE``, ``PRESERVICA``
- ``defaultArchive``: *boolean*, set to true if this archive should be treated as default archive (e.g. a REST API
  call with object context without explicit object archive specification (see also ``name``).
- ``name``: name to be used in conjunction with an object ID for REST API calls.

.. toctree::
  :maxdepth: 2

  objectarchive/filearchive
  objectarchive/preservica

