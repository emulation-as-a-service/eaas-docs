.. ClusterManager's config reference

.. include:: ../field-type-defs.txt


ClusterManager
==============

``clustermanager`` |o+r| section describes multiple configurable resource pools for emulation sessions.
This is currently the most complex configuration section.

- ``name`` |s+r|: Cluster manager's symbolic name.
- ``admin_api_access_token`` |s+r|: User-defined access token for the admin RESTful API.
  Currently it is a string consisting of arbitrary characters.
- ``providers`` |ao+r|: List of :ref:`resource provider <resource-providers>` configurations.


Example:

.. literalinclude:: examples/clustermanager.yaml
	:language: yaml


.. _resource-providers:

ResourceProviders
-----------------
.. note:: Durations are represented as integers followed by unit suffixes

	- For *Milliseconds*: ``ms``, ``msec``, ``msecs`` (e.g. ``10ms``, ``250 msecs``)
	- For *Seconds*: ``s``, ``sec``, ``secs`` (e.g. ``1s``, ``15 secs``)
	- For *Minutes*: ``m``, ``min``, ``mins`` (e.g. ``1m``, ``3 mins``)
	- For *Hours*: ``h``, ``hour``, ``hours`` (e.g. ``1h``, ``2 hours``)


.. note:: Resource specs are currently represented as objects with ``cpu`` and ``memory`` fields:

	.. code-block:: yaml

		# Resource spec representing 2 CPU cores and 8GB RAM
		node_capacity:
		    cpu: 2
		    memory: 8GB

	CPU values can be specified as full or fractional number of CPU cores:

	- ``cpu: 0.5`` will be parsed as half of a CPU core
	- ``cpu: 1`` will be parsed as one full CPU core
	- ``cpu: 2.5`` will be parsed as two and a half CPU cores

	Memory values can be specified as integers followed by unit suffixes.

	- For *Kilobytes*: ``k``, ``K``, ``kb``, ``KB`` (e.g. ``512k``, ``750KB``)
	- For *Megabytes*: ``m``, ``M``, ``mb``, ``MB`` (e.g. ``128m``, ``512MB``)
	- For *Gigabytes*: ``g``, ``G``, ``gb``, ``GB`` (e.g. ``1g``, ``2GB``)


Each of the supported resource provider types have multiple common configuration fields.

- ``name`` |s+r|: Symbolic name of the resource provider.
- ``type`` |s+r|: Provider's type name. Valid values are ``blades``, ``gce``, ``jclouds``
- ``labels`` |o+o|: Set of key/value pairs, interpreted as labels.
- ``deferred_allocations_gc_interval`` |d+o|: Interval between checks for deferred allocations with expired deadlines.
- ``request_history`` |o+o|: Parameters for history management of resource allocation requests.

	- ``update_interval`` |d+o|: Interval between statistics update.
	- ``max_request_age`` |d+o|: Max. age of requests to keep in the history.
	- ``max_number_requests`` |i+o|: Max. number of requests to keep in the history.

- ``preallocation`` |o+o|: Parameters for preallocation decisions.

	- ``request_history_multiplier`` |f+o|: Scaling factor used for calculation of resources to preallocate.
	  Values smaller than 1.0 will result in smaller amounts of preallocated resources.
	  Values greater than 1.0 will result in more aggressive preallocation.
	- ``min_bound`` |r+o|: Min. amount of resources to preallocate.
	- ``max_bound`` |r+o|: Max. amount of resources to preallocate.

.. _poolscaler:

- ``poolscaler`` |o+r|: Parameters for resource provider's poolscaler.

	- ``min_poolsize`` |i+r|: Min. number of nodes to keep in the pool.
	- ``max_poolsize`` |i+r|: Max. number of nodes allowed in the pool.
	- ``pool_scaling_interval`` |d+o|: Interval between poolscaler runs.
	- ``scaleup`` |o+o|: Parameters for node pool's scale-up actions.

		- ``max_poolsize_adjustment`` |i+o|: Max. number of nodes the pool can grow per interation.

	- ``scaledown`` |o+o|: Parameters for node pool's scale-down actions.

		- ``max_poolsize_adjustment`` |i+o|: Max. number of nodes the pool can shrink per interation.
		- ``node_warmup_period`` |d+o|: Min. amount of time after start to exclude a node from scale-down actions.
		- ``node_cooldown_period`` |d+o|: Max. amount of time in unused state to exclude a node from scale-down actions.

- ``node_allocator`` |o+r|: Parameters for resource provider's node allocator.

	- ``healthcheck`` |o+o|: Parameters for node healtchecks.

		- ``url_template`` |s+o|: HTTP URL to use for healthchecking.
		- ``connect_timeout`` |d+o|: Max. time for connecting to node.
		- ``read_timeout`` |d+o|: Max. time for reading from node.
		- ``failure_timeout`` |d+o|: Time in unreachable state to mark a node as failed.
		- ``interval`` |d+o|: Time between healtcheck runs.
		- ``num_parallel_requests`` |i+o|: Max. number of healthchecks to execute in parallel.


Additional resource provider's type-specific configuration fields are described in the corresponding
subsections (see :ref:`blades <blades-provider>`, :ref:`gce <gce-provider>` and :ref:`jclouds <jclouds-provider>`).


.. _blades-provider:

Blades
^^^^^^
Resource providers of type ``blades`` support the following configuration parameters in addition to parameters
described in :ref:`previous section <resource-providers>`:

- ``node_allocator`` |o+r|:

	- ``node_capacity`` |r+r|: Capacity of a single node in the node pool.
	- ``node_addresses`` |as+r|: List of IP addresses of all nodes, that should be available
	  in the resource provider's node pool.


Example:

.. literalinclude:: examples/node-allocator-blades.yaml
	:language: yaml


.. _gce-provider:

Google Compute Engine
^^^^^^^^^^^^^^^^^^^^^
Resource providers of type ``gce`` support the following configuration parameters in addition to parameters
described in :ref:`previous section <resource-providers>`:

- ``node_allocator`` |o+r|:

	- ``application_name`` |s+o|: Symbolic name of the GCE application.
	- ``project_id`` |s+r|: ID of the `GCE project <https://cloud.google.com/resource-manager/docs/creating-managing-projects>`_.
	- ``zone_name`` |s+r|: Name of the zone where the node pool should be located. Currently available zones are
	  listed `here <https://cloud.google.com/compute/docs/regions-zones/regions-zones#available>`_.
	- ``network_name`` |s+r|: Name of the network to use for the node pool. It is expected, that this network exists
	  and is configured (see `this overview <https://cloud.google.com/compute/docs/vpc/>`_).
	- ``node_name_prefix`` |s+o|: Prefix to use for nodes added to the node pool.
	- ``credentials_file`` |s+r|: Absolute path to a JSON file containing
	  `credentials <https://cloud.google.com/compute/docs/access/>`_ for the project specified with ``project_id``.
	  To get this file follow steps *a-f* from `this guide <https://developers.google.com/identity/protocols/application-default-credentials#howtheywork>`_.

	- ``vm`` |o+r|: Parameters for allocation of new VMs.

		- ``machine_type`` |s+r|: Name of the machine type, as specified `here <https://cloud.google.com/compute/docs/machine-types>`_.
		- ``persistent_disk`` |o+r|: Parameters for the main `persistent disk <https://cloud.google.com/compute/docs/disks/#pdspecs>`_.

			- ``type`` |s+r|: Type of the boot disk. Use either ``pd-standard`` for hard-disk drives or
			  ``pd-ssd`` for solid-state drives.
			- ``size`` |i+r|: Size of the disk in GB.
			- ``image_url`` |s+r|: URL to a boot image.

		- ``accelerators`` |ao+o|: List of `GPUs <https://cloud.google.com/compute/docs/gpus/>`_ to attach
		  to each node. Each entry must provide the following fields:

			- ``type`` |s+r|: Type or name of the accelerator. Currently only ``nvidia-tesla-k80`` is supported.
			- ``count`` |i+r|: Number of accelerators to add to each node.

		- ``boot_poll_interval`` |d+o|: Time between checks for VM readiness during boot.
		- ``boot_poll_interval_delta`` |d+o|: Randomization factor for checks during VM boot.
		- ``max_num_boot_polls`` |d+o|: Max. number of boot checks to perform. If the VM is
		  still not ready after that, it will be marked as failed.

	- ``api`` |o+o|: Parameters for accessing rate-limited APIs

		- ``poll_interval`` |d+o|: Time between polling for results of async-operations.
		- ``poll_interval_delta`` |d+o|: Randomization factor for polling.
		- ``retry_interval`` |d+o|: Time between rate-limited requests.
		- ``retry_interval_delta`` |d+o|: Randomization factor for rate-limited requests.
		- ``max_num_retries`` |i+o|: Max. number of retries to consider a rate-limited request as failed.


Example:

.. literalinclude:: examples/node-allocator-gce.yaml
	:language: yaml


.. _jclouds-provider:

JClouds
^^^^^^^
Resource providers of type ``jclouds`` currently support only *OpenStack*-based backends. The following
configuration parameters in addition to parameters described in :ref:`previous section <resource-providers>`
can be specified:

- ``node_allocator`` |o+r|:

	- ``security_group_name`` |s+r|: Name of the security group to use for the node pool.
	- ``node_group_name`` |s+o|: Symbolic name of the node pool.
	- ``node_name_prefix`` |s+o|: Prefix to use for nodes created in the node pool.
	- ``provider`` |o+r|: Backend provider specific settings.

		- ``name`` |s+r|: Name of the backend. Currently ``openstack-nova``
		- ``identity`` |s+r|: Tenant-ID, followed by ``:`` and full username as provided by the backend
		  (see the example below).
		- ``credential`` |s+r|: User's password for authentication.
		- ``endpoint`` |s+r|: Backend's service endpoint.
 
	- ``vm`` |o+r|: Parameters for allocation of new VMs.

		- ``network_id`` |s+r|: ID of the network to add VMs to.
		- ``hardware_id`` |s+r|: ID of the hardware flavor.
		- ``image_id`` |s+r|: ID of the boot image.
		- ``boot_poll_interval`` |d+o|: Time between checks for VM readiness during boot.
		- ``boot_poll_interval_delta`` |d+o|: Randomization factor for checks during VM boot.
		- ``max_num_boot_polls`` |i+o|: Max. number of boot checks to perform. If the VM is
		  still not ready after that, it will be marked as failed.


Example:

.. literalinclude:: examples/node-allocator-jclouds.yaml
   :language: yaml

Defaults
--------

.. literalinclude:: examples/clustermanager-defaults.yaml
   :language: yaml
