.. include:: ../field-type-defs.txt


Image Proposer
==============

``imageproposer`` |o+r| section describes image proposer specific settings.

- ``sorting_config_file`` |s+o|: Absolute path to a configuration file for :ref:`sorting images <image-sorting>`
  in proposals. When not set, then image sorting will be disabled.


Example:

.. literalinclude:: examples/imageproposer.yaml
	:language: yaml


.. _image-sorting:

Image Sorting
-------------

The order of images in the proposal response can be influenced by a special configuration file in YAML format.
In that file a list of image's properties or fields must be specified, by which the images sould be sorted.
Then a mapping must be provided, that assigns a rank value (an integer) to each specific field's value.
The proposer then uses that information to sort images using the following comparison-algorithm::

	compare_images_by_rank(image1, image2):
		foreach field in config.sort_by:
			rank1 = lookup_rank(config, field, image1)
			rank2 = lookup_rank(config, field, image2)
			result = compare(rank1, rank2, order)
			if (result != "EQUAL")
				return result

		# All fields have equal ranks!
		return "EQUAL"



The following configuration parameters are currently supported:

``sort_by`` |ao+r| section describes fields to sort images by and their order.

- ``field`` |s+r|: The field's name. Currently only ``id`` is implemented.
- ``order`` |s+r|: The field's sort ordering. Valid values are ``ascending`` and ``descending``.


``ranks`` |ao+r| section describes rank assignments for specific field's values.

- ``field`` |s+r|: The field's name as defined in ``sort_by``.
- ``default`` |i+r|: The field's default rank.
- ``mapping`` |ao+r|: The mapping from field's specific values to their ranks.

	- ``value`` |s+r|: The field's value to assign a rank for.
	- ``rank`` |i+r|: The corresponding rank value.


Example:

.. literalinclude:: examples/image-proposal-sorting.yaml
	:language: yaml

If an image proposal contains images with IDs ``123``, ``456``, ``789`` and ``ABC``, then their order according
to the example configuration would be: ``789`` (rank 30), ``456`` (rank 20), ``123`` (rank 10), ``ABC`` (default rank 0)


Defaults
--------

.. literalinclude:: examples/imageprop-defaults.yaml
  :language: yaml

