.. include:: ../field-type-defs.txt


EmuComp
=======

EmuComp specific configuration settings:

Defaults
--------

.. literalinclude:: examples/emucomp-defaults.yaml
   :language: yaml
