.. include:: ../field-type-defs.txt


Image Classifier
================

In image classifier specific settings.

Defaults
--------

.. literalinclude:: examples/imagecls-defaults.yaml
  :language: yaml
