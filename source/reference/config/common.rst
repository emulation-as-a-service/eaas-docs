.. include:: ../field-type-defs.txt


System (Common)
===============

In EaaS misc system settings.

Defaults
--------

.. literalinclude:: examples/common-defaults.yaml
   :language: yaml
