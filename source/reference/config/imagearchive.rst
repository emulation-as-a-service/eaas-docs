.. include:: ../field-type-defs.txt


Image Archive
=============

In image archive specific settings.

Defaults
--------

.. literalinclude:: examples/imagearchive-defaults.yaml
  :language: yaml
