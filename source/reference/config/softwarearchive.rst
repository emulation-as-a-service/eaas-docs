.. include:: ../field-type-defs.txt


Software Archive
================

In Software Archive specific settings.

Defaults
--------

.. literalinclude:: examples/softwarearchive-defaults.yaml
  :language: yaml
