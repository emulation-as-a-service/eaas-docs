.. include:: ../field-type-defs.txt


Components
==========

In EaaS terms components are functional elements (e.g. emulators).

Defaults
--------

.. literalinclude:: examples/components-defaults.yaml
   :language: yaml
