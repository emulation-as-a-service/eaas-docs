.. include:: ../field-type-defs.txt


Object Archive
==============

In Object Archive specific settings.

Defaults
--------

.. literalinclude:: examples/objectarchive-defaults.yaml
  :language: yaml
