.. include:: ../field-type-defs.txt


EMIL / Demo UI
==============

In Emil / UI specific settings.

Defaults
--------

.. literalinclude:: examples/emil-defaults.yaml
  :language: yaml
