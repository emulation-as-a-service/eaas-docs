.. include:: ../field-type-defs.txt


Preservica Adapter
==================

The Preservica Adapter uses the Preservica SDB REST interface to retrieve object meta-data and their
manifestations.

.. note:: Currently no random access to object manifestation data (e.g. ISOs) is possible. Therefore,
  images are downloaded (might take some time) and cached for fast subsequent access. EaaS does not enforce
  disk / space quotas on the filesystem hosting the cached images. Make sure to provide enough cache space and
  monitor space allocation.

.. note:: Currently each Preservica adapter configuration requires a single ``collection`` ID containing a
  flat hierarchy of ``Deliverable Unit`` which represents a renderable / usable ``object`` within EaaS and
  its unit ID will be used as ``objectID`` as internal metadata.


Example
-------

.. literalinclude:: examples/preservica.json
   :language: json
