

File Archive Adapater
=====================

The default file adapter is the default object archive implementation. This backend is also used
for local object caches.

The configuration file contains only a single field:

- ``localPath``: path to the object's folder.

The objects folder contains the individual objects. Each logical ``objectId`` is represented as a file system folder.
This folder contains the object's manifestation (files) and metadata.

To use the object manifestations within EaaS their mediatypes have to be made explicit. Currently this is done
by copying the objects in special subfolders. The following types are currently supported:

   - ``iso/`` This folder should contain CDROM disk images.
   - ``disk/`` This folder should contain hard disk images.
   - ``floppy/`` This folder should contain floppy disk images.
   - ``file/`` This folder may contain arbitrary files (including subfolder). These files will be wrapped as
     ISO/CDROM image by default. Note: the folder will not be checked for updated files, i.e. the contents of this folder will
     not be “repackaged” automatically. To force, repackaging, delete the `__imported.iso` file in the ``iso/`` folder of the current object.
