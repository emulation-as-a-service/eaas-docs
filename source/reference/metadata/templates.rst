
Templates
---------

Emulation Environment Templates are structurally mostly identical to the ``EmulationEnvironment`` data type but are
defined as own type:::

  <emulationEnvironmentTemplate xmlns="http://bwfla.bwl.de/common/datatypes">
  ...

Templates are emulation environments without "data", i.e. no disk image is defined. For instance by using the
import workflow a configure emulation environment can be *bound* to an external disk image.

Some images require additional processing during import, in EaaS terms **image generalization**, i.e. adaptation
of the image's driver configuration.

The following image generalization example runs a shell script if the ``precondition`` matches. In this example
the precondition is an NTFS partition with the Windows registry and driver folders present::

  <imageGeneralization>
    <modificationScript>patch.sh</modificationScript>
    <precondition>
       <fileSystem>ntfs</fileSystem>
       <partitionLabel></partitionLabel>
       <requiredFiles>
         <fileName>/WINDOWS/system32/config/system</fileName>
         <fileName>/WINDOWS/system32/drivers</fileName>
       </requiredFiles>
     </precondition>
  </imageGeneralization>

