.. Emulation Environment

.. include:: ../field-type-defs.txt

Emulation Environments
======================

An emulation environment describes a complete emulation setup:

.. image:: images/emulation-environment.png

The current EaaS implementation of an emulation environment has the following structure:

.. literalinclude:: examples/env-structure.xml
   :language: xml

.. note::
   This schema is not stable and may be updated. With introduction of version tags
   the EaaS framework may provide compat layers and/or automated conversion.

Header Section
--------------
The emulation environment's header components define fields common to all emulators:

- ``id`` |s+r| : unique identifier of the environment
- ``description``: descriptive information

  - ``title`` |s+r|
  - ``os`` |s+r|

- ``arch`` |e+r|: currently supported only be Qemu: ``i386``, ``ppc``
- ``emulator`` |s+r|: a EaaS EmulatorBean implementation. An emulator bean integrates a specific emulator into the framework.

  - Supported beans:

    - BasiliskII (example)
    - Beebem (example)
    - DosBox (examples: [Doom](./examples/doom.env))
    - Hatari (example: [Atari](.examples/hatari_tOS206us.env))
    - Kegs (example)
    - PceAtari (example)
    - PceIbmPc (example)
    - PceMacPlus (example)
    - Qemu (examples: [Dos 6.2](./examples/dos620_cdrom.env), [Win 3.11](./examples/win311.env))
    - SheepShaver (examples: [MacOS9](./examples/macos9.env))
    - ViceC128 (example)
    - ViceC64 (example)

**Example:**

.. literalinclude:: examples/env-header.xml
  :language: xml


UI Options
----------
The UI Options allow to configure presentation features.

- ``HTML5``: HTML5 presentation options

  - ``pointer_lock`` (boolean): Enables the HTML5 pointer lock (`more information here <https://developer.mozilla.org/en-US/docs/Web/API/Pointer_Lock_API>`_.
    Useful if the emulator or the operating system do not support absolute mouse positions (e.g. Windows 3.11).
  - ``clientKbdLayout`` (String): the user's keyboard layout.

**Example:**

.. literalinclude:: examples/env-ui-options.xml
   :language: xml


Drive Definitions
-----------------

Drive definitions define usable drives of for an emulation environment. Drives may have empty ``data``
tags (e.g. to define an empty CDROM drive to be bound by a workflow) or predefined data sources (c.f. the Bindings section).

- ``data`` |s+o|: URL to a data source. Supported URL types are: http, nbd, binding, file.
- ``type`` (Enumeration): Drive types: cdrom, floppy, disk, iso
- ``iface``, ``bus``, ``unit``: Emulator / bean specific, sees examples.

**Example**, defining three drives (floppy, cdrom and disk). The floppy and cdrom drive are unbound yet:

.. literalinclude:: examples/env-drives.xml
   :language: xml

Bindings
--------
Bindings describes/defines data sources.

- ``id`` |s+r|: ID of the binding to be referenced within an environment.
- ``url`` |s+r|: URL to a data source.
- ``access`` (Enumeration): copy or cow (copy on write).

**Example**, creates a new "main_hdd" binding to be used with a drive.
The url references to an image file stored in the configured image-archive::

 <binding id="main_hdd">
  <url>imagearchive:qemu-i386-DOS_6.20_CDROM.raw</url>
  <access>cow</access>
 </binding>


Native Config
-------------
The native config allows to pass emulator specific options to the emulator. See SheepShaver examples.
